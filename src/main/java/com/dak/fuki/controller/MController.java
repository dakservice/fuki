/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.controller;

import com.dak.fuki.entity.MLogMobile;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.HttpServletEntity;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import com.dak.fuki.utility.Tripledes;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Frika Da Cintia
 */
@RestController
//@RequestMapping(value = "/fuki/{service_type}", method = RequestMethod.POST)
public class MController {
    
    Log log = Log.getLog("Q2", getClass().getName());

    private ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
    private MRc mapRc = new MRc();
    Context ctx = new Context();
    Space sp = SpaceFactory.getSpace();

    @RequestMapping(value = "/fuki/{service_type}", method = RequestMethod.POST)
    public String allProcess(@RequestBody(required = true) String body, @PathVariable("service_type") String path,
            HttpServletRequest request) throws ParseException {
    
        HttpServletEntity a = new HttpServletEntity(request);

//        if (httpServletEntity.getAuthorize().length() == 24) {
            MLogMobile mActivity = new MLogMobile();
            mActivity.setRemoteAddr(a.getRemoteAddr());
            mActivity.setRequestUrl(a.getRequestUrl());
            mActivity.setRouterPath(path);
            mActivity.setRequestBody(body);
            mActivity.setRequestTime(new Date());
            mActivity.setToken(a.getAuthorize());
            mActivity = SpringInit.getmLogMobileDao().saveOrUpdate(mActivity);

//            Tripledes tDes = new Tripledes(mActivity.getToken());

            log.info("Incoming from : " + mActivity.getRemoteAddr());
            log.info("Incoming Request Url : " + mActivity.getRequestUrl());
            log.info("Incoming Body: " + mActivity.getRequestBody());

            JSONObject reqBody = new JSONObject(mActivity.getRequestBody());
            try {
                Space sp = SpaceFactory.getSpace();
                
                ctx.put(Constant.WS.PATH, mActivity.getRouterPath());
                ctx.put(Constant.WS.REQUEST_BODY, reqBody);
                ctx.put(Constant.WS.REQUEST, mActivity);
                ctx.put(Constant.SETTING.SETTING_DESC.TRXMGR, Constant.SETTING.SETTING_VALUE.TRXMGR);
                ctx.put(Constant.SETTING.SETTING_DESC.TIME_OUT, Constant.SETTING.SETTING_VALUE.TIME_OUT);
                sp.out(Constant.SETTING.SETTING_VALUE.TRXMGR, ctx, Constant.SETTING.SETTING_VALUE.TIME_OUT);

//                Send request and get response from space
                Context response = (Context) sp.in(mActivity.getId(), Constant.SETTING.SETTING_VALUE.TIME_OUT);
                log.info("Insert request " + mActivity.getId() + " to space[" + Constant.SETTING.SETTING_VALUE.TRXMGR + "], timeout trx : " + Constant.SETTING.SETTING_VALUE.TIME_OUT);
                log.info("Waiting " + mActivity.getId() + " from space");
                log.info("Get " + mActivity.getId() + " from space");
//                Response action handle
                if (response != null) {
                    mActivity.setStatus(response.getString(Constant.TRX.STATUS));
                    mapRc = (MRc) response.get(Constant.TRX.RC);
                    wsResponse = (ResponseWebServiceContainer) response.get(Constant.WS.RESPONSE);
                } else {
                    mActivity.setStatus(Constant.WS.STATUS.FAILED);
                    mapRc = SpringInit.getmRcDao().getRc(Constant.WS.RC.TIMEOUT);
                    wsResponse = new ResponseWebServiceContainer(mapRc.getRc(), mapRc.getRm(), reqBody);

                }

            } catch (Exception ex) {
                log.error(ExceptionUtils.getStackTrace(ex));
                mActivity.setStatus(Constant.WS.STATUS.FAILED);
                mapRc = SpringInit.getmRcDao().getRc(Constant.WS.RC.SERVICE_PATH_NOT_FOUND);
                wsResponse = new ResponseWebServiceContainer(mapRc.getRc(), mapRc.getRm(), reqBody);
            }
            
            mActivity.setResponseTime(new Date());
//            mActivity.setResponse(tDes.encrypt(wsResponse.jsonToString()));
            mActivity.setResponseCode(mapRc.getRc());
            mActivity.setResponseMessage(mapRc.getRm());


            log.info("Outgoing to : " + request.getRemoteAddr());
            log.info("Outgoing Body: " + wsResponse.jsonToString());

            SpringInit.getmLogMobileDao().saveOrUpdate(mActivity);
            return wsResponse.jsonToString();
//        } else {
//            mapRc = SpringInit.getmResponseDao().getRc(Constant.WS.RC.AUTH_FAILED);
//            wsResponse = new ResponseWebServiceContainer(mapRc.getRc(), mapRc.getRm());
//
//            return wsResponse.jsonToString();
//        }

    }

}
