/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.tx.participant;

import com.dak.fuki.entity.MLogTrx;
import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.MTransSP;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class GetSaldos implements TransactionParticipant{
    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DecimalFormat df = new DecimalFormat("#");
        
        try {
            Integer saldo;

            String notelp = jsonReq.getString("notelp");
            log.info("========================= NO TELEPON " +notelp);
            JSONArray data = new JSONArray();
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);           

            if (nsbh == null) {
                log.info("====================== DATA" +nsbh);
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            } else {
                    
                Integer saldo_spokok = SpringInit.getmNasabahDao().getSaldoPokoks(nsbh.getId().toString());
                Integer saldo_swajib = SpringInit.getmNasabahDao().getSaldoWajibs(nsbh.getId().toString());
                Integer saldo_ssukarela = SpringInit.getmNasabahDao().getSaldoSukarelas(nsbh.getId().toString());

                Integer s_pokok;
                Integer s_wajib;
                Integer s_sukarela;

                if(saldo_spokok > 0){
                    s_pokok = saldo_spokok;
                }else{
                    s_pokok = 0;
                }

                if(saldo_swajib > 0){
                    s_wajib = saldo_swajib;
                }else{
                    s_wajib = 0;
                }

                if(saldo_ssukarela > 0){
                    s_sukarela = saldo_ssukarela;
                }else{
                    s_sukarela = 0;
                }

                saldo = s_pokok + s_wajib + s_sukarela;
                log.info("====== SALDO : "+saldo+" POKOK : "+s_pokok+" + WAJIB : "+s_wajib+" + SUKARELA "+s_sukarela);

                JSONObject json = new JSONObject();
                json.put("username", notelp);
                json.put("nama", nsbh.getNama());
                json.put("saldo", ""+df.format(saldo));
                json.put("simpanan_wajib", ""+df.format(s_wajib));
                log.info("====================== JUMLAH SIM WAJIB " +df.format(s_wajib));
                json.put("simpanan_pokok", ""+df.format(s_pokok));
                log.info("====================== JUMLAH SIM POKOK " +df.format(s_pokok));
                json.put("simpanan_sukarela", ""+df.format(s_sukarela));
                log.info("====================== JUMLAH SIM SUKARELA " +df.format(s_sukarela));
                JSONObject json2 = new JSONObject();
                json2.put("data", json);
                log.info("=============================== LOG DATA :"+json2);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++"+stringWriter.toString());
            
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
