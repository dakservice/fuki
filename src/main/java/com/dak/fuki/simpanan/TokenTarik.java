/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.simpanan;

import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.MTokenSetorTarik;
import com.dak.fuki.entity.MTokenSetorTarikDetail;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import com.dak.fuki.utility.Utility;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class TokenTarik implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    
    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    Calendar cal = Calendar.getInstance();
    
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        Date expire;
        
        try {
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            Integer id_simpanan = jsonReq.getInt("id_simpanan");
//            String saldo = jsonReq.getString("saldo");
            Integer nominal = jsonReq.getInt("nominal");
            String token = Utility.generateToken(6);
            
            cal.add(Calendar.MINUTE, 30);
            expire = cal.getTime();
//            Integer date = new Date().getMinutes();
//            Integer hasilmenit = date +30;
//            log.info("============== HASIL GET MINUTES "+new Date().getMinutes());
//            log.info("============== HASIL PLUS MINUTES "+hasilmenit);
            
            
            if(nsbh != null){
                if(nsbh.getAktif().equals("Y")){
//                    if(Integer.parseInt(saldo) >= nominal){
                        MTokenSetorTarik mtarik = new MTokenSetorTarik();
                        mtarik.setToken_number(token);
                        mtarik.setExpired_date(time.format(expire));
                        mtarik.setAction("PENARIKAN");
                        mtarik.setStatus("0");
                        mtarik.setId_user(nsbh.getId().toString());
                        mtarik.setTanggal_trx(datetime.format(new Date()));

                        SpringInit.getMSetorTarikDao().saveOrUpdate(mtarik);


                        MTokenSetorTarikDetail mtarikdetail = new MTokenSetorTarikDetail();
                        mtarikdetail.setToken_id(mtarik.getId());
                        mtarikdetail.setId_simpanan(id_simpanan);
                        mtarikdetail.setNominal(nominal);

                        SpringInit.getMSetorTarikDao().saveOrUpdate(mtarikdetail);

                        JSONObject js = new JSONObject();
                        js.put("total_pembayaran", nominal.toString());
                        js.put("token", token);
                        js.put("expire_date", time.format(expire));

                        JSONObject json = new JSONObject();
                        json.put("data", js);
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);
//                    }else{
//                        rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.BALANCE_NOT_SUFFICIENT);
//                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
//                    }
                }else{
                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            }else{
                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {
             StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
