/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.dao;

import com.dak.fuki.entity.MJenisSimpan;
import com.dak.fuki.entity.MJurnal;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Frika Da Cintia
 */
@Repository(value = "MJenisSimpanDao")
@Transactional
public class MJenisSimpanDao extends Dao{
    
    public MJenisSimpan jenisSimpanById(Integer id) {
        try {
            return (MJenisSimpan) em.createQuery("SELECT mo FROM MJenisSimpan mo WHERE mo.id=:id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MJenisSimpan> getAllJenisSimpanan() {
        try {
           return em.createQuery("SELECT mo FROM MJenisSimpan mo ORDER BY mo.id ASC ")
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MJenisSimpan> getJenisSimpanan() {
        try {
           return em.createQuery("SELECT mo FROM MJenisSimpan mo where mo.tarik_tunai !='T'")
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
