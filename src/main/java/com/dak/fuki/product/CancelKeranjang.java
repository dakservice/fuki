/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.product;

import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.TmpDetailBrg;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class CancelKeranjang implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        
        try {
            String id_barang = jsonReq.getString("id_barang");
            String notelp = jsonReq.getString("notelp");
            String jenis_pengajuan = jsonReq.getString("jenis_pengajuan");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            List<Object[]> tdb = SpringInit.getTmpDetailBrgDao().getAllChartNative(nsbh.getId().longValue());
            List<Object[]> tdc = SpringInit.getTmpDetailChartDao().getAllChartNative(nsbh.getId().longValue());
            if(nsbh != null ){
                if(nsbh.getAktif().equals("Y")){
                    if(tdb.size() > 0 || tdc.size() > 0){
                        if(jenis_pengajuan.toLowerCase().equals("pengajuan barang")){
                            log.info("======= MASUK PENGAJUAN BARANG");
                            SpringInit.getTmpDetailBrgDao().deleteCartBrg(Integer.parseInt(nsbh.getId().toString()), Integer.parseInt(id_barang));
                        }else{
                            log.info("======= MASUK PENGAJUAN MOTOR");
                            SpringInit.getTmpDetailChartDao().deleteCartMtr(Integer.parseInt(nsbh.getId().toString()), Long.parseLong(id_barang));
                        }
                        
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    }else{
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.DATA_NOT_FOUND);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    }
                }else{
                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            }else{
                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
            
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
