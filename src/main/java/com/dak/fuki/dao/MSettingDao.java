/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.dao;

import com.dak.fuki.entity.MSetting;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Frika Da Cintia
 */
@Repository(value = "mSettingDao")
@Transactional(transactionManager = "transactionManagerInternal")
public class MSettingDao extends Dao{
    
    public MSetting getSetting(String incom) {
        try {
            return (MSetting) em.createQuery("SELECT mrc FROM MSetting mrc WHERE mrc.incoming = :incoming")
                    .setParameter("incoming", incom)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
