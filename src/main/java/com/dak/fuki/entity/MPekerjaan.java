/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Frika Da Cintia
 */
@Entity(name = "MPekerjaan")
@Table(name = "pekerjaan")
public class MPekerjaan {

    @Id
    @Column(name = "id_kerja", nullable = false)
    private String id_kerja;
    
    @Column(name = "jenis_kerja", nullable = false)
    private String jenis_kerja;

    public String getId_kerja() {
        return id_kerja;
    }

    public void setId_kerja(String id_kerja) {
        this.id_kerja = id_kerja;
    }

    public String getJenis_kerja() {
        return jenis_kerja;
    }

    public void setJenis_kerja(String jenis_kerja) {
        this.jenis_kerja = jenis_kerja;
    }

    

    
}
