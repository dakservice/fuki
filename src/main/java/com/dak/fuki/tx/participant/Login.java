/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.tx.participant;

import com.dak.fuki.entity.MLogTrx;
import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.MTransSP;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import com.dak.fuki.utility.Tripledes;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class Login implements TransactionParticipant{
    
//    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        
        try {
//            Tripledes tdes = new Tripledes(Constant.SETTING.TRIDES_MOB_ENCRYPT);
            String notelp = jsonReq.getString("notelp");
            String pass = "nsi"+jsonReq.getString("password");
            String password = SpringInit.getmNasabahDao().encryptPasswordSHA1(pass);
            System.out.println("No Telepon " + notelp);
            log.info("========================= NO TELEPON " +notelp);
            log.info("========================= PASSWORD " +password);
            
            MNasabah nsbh = SpringInit.getmNasabahDao().loginUser(notelp, password);
            log.info("====================== DATA NSBH " +nsbh);
            
            if (nsbh == null) {
                log.info("====================== LOGIN FAILED");
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_FAILED);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                ctx.put(Constant.LOGIN.STATUS_LOGIN, Constant.WS.RC.LOGIN_FAILED);
//                log.info("====================== STATUS LOGIN" +ctx.get(Constant.LOGIN.STATUS_LOGIN, rc));

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return  ABORTED | NO_JOIN;

            } else if (nsbh.getAktif().equals("N")) {
                log.info("====================== USER NOT ACTIVE");

                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                
                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
                
                return  ABORTED | NO_JOIN;

            } else {
                log.info("====================== LOGIN SUKSES");
                JSONObject json = new JSONObject();
                json.put("username", nsbh.getNotelp());
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                
                JSONObject json2 = new JSONObject();
                json2.put("data", json);
                
                
                ctx.put(Constant.USER_DETAIL, nsbh);
                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
                
                return  PREPARED | NO_JOIN;

            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
            return  ABORTED | NO_JOIN;
        }
        
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        
        
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

    
}
