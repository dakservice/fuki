package com.dak.fuki.dao;

import javax.annotation.Resource;
import javax.persistence.EntityManager;

/**
 *
 * @author risyamaulana
 */
public class Dao {

    @Resource(name = "sharedEntityManagerInternal")
    protected EntityManager em;
}
