/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.dao;

/**
 *
 * @author Frika Da Cintia
 */


import com.dak.fuki.entity.MJenisAngsuran;
import com.dak.fuki.entity.MJenisPinjaman;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value = "MJenisAngsuranDao")
@Transactional
public class MJenisAngsuranDao extends Dao{
    
    public List<MJenisAngsuran> viewJenisAngsuran() {
        try {
            List mjp;
            mjp = em.createQuery("SELECT mjp from MJenisAngsuran mjp WHERE mjp.aktif='Y' ORDER BY mjp.ket ASC")
                    .getResultList();
            return mjp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
