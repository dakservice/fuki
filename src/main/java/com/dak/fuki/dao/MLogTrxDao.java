/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.dao;

import com.dak.fuki.entity.MLogTrx;
import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MProduct;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Frika Da Cintia
 */
@Repository(value = "MLogTrxDao")
@Transactional
public class MLogTrxDao extends Dao{
    
    public MLogTrx saveOrUpdate(MLogTrx mLogTrx) {
        if (mLogTrx.getId_logtrx() == null) {
            em.persist(mLogTrx);
        } else {
            em.merge(mLogTrx);
        }
        return mLogTrx;
    }
    
    
    public List<MLogTrx> getHistorySaldo(String anggota_id) {
        try {
           return em.createQuery("SELECT mlt FROM MLogTrx mlt WHERE mlt.anggota_id =:anggota_id ORDER BY mlt.id_logtrx DESC")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<Object[]> getHistoryByDate(String anggota_id, String strdate, String enddate) {
        try {
           return em.createNativeQuery("SELECT * FROM MLogTrx WHERE anggota_id = :anggota_id and created_time >= DATE(:strdate) and created_time <= DATE(:enddate)")
                    .setParameter("anggota_id", anggota_id)
                    .setParameter("strdate", strdate)
                    .setParameter("enddate", enddate)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
