/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.laporan;

import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.MTransSP;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class LapSimpanTarik implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DecimalFormat df = new DecimalFormat("#");
        
        try {
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            
            if(nsbh != null ){
                if(nsbh.getAktif().equals("Y")){
                    List <MTransSP> msimpanan = SpringInit.getMTransSPDao().getAll(Integer.parseInt(nsbh.getId().toString()));
                    
                    if(msimpanan.size() > 0){
                    JSONArray data = new JSONArray();
                    for (MTransSP datamp : msimpanan) {
                        JSONObject logData = new JSONObject();
                        logData.put("tanggal", datamp.getTgl_transaksi());
                        logData.put("jenis", datamp.getAkun());
                        logData.put("jumlah", df.format(datamp.getJumlah()));
                        logData.put("keterangan", datamp.getKeterangan());

                        data.put(logData);

                        jsonReq.put("data", data);
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
                    }
                    }else{
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.DATA_NOT_FOUND);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    }
                }else{
                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            }else{
                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
