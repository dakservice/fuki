/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.simpanan;

import com.dak.fuki.entity.MJenisSimpan;
import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class initFormSetor implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DecimalFormat df = new DecimalFormat("#");
        JSONArray arrays = new JSONArray(); 
        
        try {
            
            String notelp = jsonReq.getString("notelp");
            
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);           

            if (nsbh != null) {
                if(nsbh.getAktif().equals("Y")){
                
                    List<MJenisSimpan> mjss = SpringInit.getMJenisSimpanDao().getAllJenisSimpanan();
                    for (MJenisSimpan datams : mjss) {
                        JSONObject logData = new JSONObject();
                        logData.put("id", datams.getId());
                        logData.put("nama_simpanan", datams.getJns_simpan());
                        logData.put("input_value", df.format(datams.getJumlah()).toString());
                        
                        String s_custom;
                        if(df.format(datams.getJumlah()).toString().equals("0")){
                            s_custom = "1";
                        }else{
                            s_custom = "0";
                        }
                        logData.put("custom_input", s_custom);
                        
                        arrays.put(logData);
                    }
                    JSONObject json = new JSONObject();
                    json.put("jenis_simpanan", arrays);
//                json.put("saldo", df.format(saldo).toString());
                
                    JSONObject json2 = new JSONObject();
                    json2.put("data", json);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
                    
                }else{
                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            }else{
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
            
        } catch (Exception e) {
            
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++"+stringWriter.toString());
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
