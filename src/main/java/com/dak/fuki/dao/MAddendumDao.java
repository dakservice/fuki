/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.dao;

import com.dak.fuki.entity.MAddendum;
import com.dak.fuki.entity.MBuktiJaminan;
import com.dak.fuki.entity.MDetailPinjaman;
import com.dak.fuki.entity.MPengajuan;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Frika Da Cintia
 */
@Repository(value = "MAddendumDao")
@Transactional
public class MAddendumDao extends Dao{
    
        public MAddendum saveOrUpdate(MAddendum mAddendum) {
        if (mAddendum.getId() == null) {
            em.persist(mAddendum);
        } else {
            em.merge(mAddendum);
        }
        return mAddendum;
    }
    
        
    public List<MAddendum> getAllById(Integer id_pinjam) {
        try {
           return em.createQuery("SELECT mt FROM MAddendum mt WHERE mt.pinjam_id = :id_pinjam")
                    .setParameter("id_pinjam", id_pinjam)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public MAddendum getAddendumById() {
        try {
            MAddendum mp = (MAddendum) em.createQuery("SELECT mp FROM MAddendum mp ORDER BY mp.id DESC ")
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
}
