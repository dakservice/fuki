/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.spring;

import com.dak.fuki.dao.MAddendumDao;
import com.dak.fuki.dao.MBuktiJaminanDao;
import com.dak.fuki.dao.MDPinjamanDao;
import com.dak.fuki.dao.MDetailPinjamanDao;
import com.dak.fuki.dao.MHeadPinjamanDao;
import com.dak.fuki.dao.MJenisAngsuranDao;
import com.dak.fuki.dao.MJenisPinjamanDao;
import com.dak.fuki.dao.MJenisSimpanDao;
import com.dak.fuki.dao.MJurnalDao;
import com.dak.fuki.dao.MLogMobileDao;
import com.dak.fuki.dao.MLogTrxDao;
import com.dak.fuki.dao.MNasabahDao;
import com.dak.fuki.dao.MNilaiJaminanDao;
import com.dak.fuki.dao.MPengajuanDao;
import com.dak.fuki.dao.MPresentasePlafonDao;
import com.dak.fuki.dao.MProductDao;
import com.dak.fuki.dao.MProfileDao;
import com.dak.fuki.dao.MRcDao;
import com.dak.fuki.dao.MRoutingDao;
import com.dak.fuki.dao.MSetorTarikDao;
import com.dak.fuki.dao.MSettingDao;
import com.dak.fuki.dao.MStatusPengajuanDao;
import com.dak.fuki.dao.MSukuBungaDao;
import com.dak.fuki.dao.MTransSPDao;
import com.dak.fuki.dao.TmpDetailBrgDao;
import com.dak.fuki.dao.TmpDetailChartDao;
import javax.naming.ConfigurationException;
import org.jpos.util.Log;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Frika Da Cintia
 */
public class SpringInit {
     Log log = Log.getLog("Q2", getClass().getName());
    
    private static MNasabahDao mNasabahDao;
    private static MRcDao mRcDao;
    private static MSettingDao mSettingDao;
    private static MLogMobileDao mLogMobileDao;
    private static MPengajuanDao MPengajuanDao;
    private static MProductDao MProductDao;
    private static MLogTrxDao MLogTrxDao;
    private static MRoutingDao MRoutingDao;
    private static TmpDetailChartDao TmpDetailChartDao;
    private static MTransSPDao MTransSPDao;
    private static MJurnalDao MJurnalDao;
    private static MJenisSimpanDao MJenisSimpanDao;
    private static MJenisAngsuranDao MJenisAngsuranDao;
    private static MJenisPinjamanDao MJenisPinjamanDao;
    private static MPresentasePlafonDao MPresentasePlafonDao;
    private static MSukuBungaDao MSukuBungaDao;
    private static MStatusPengajuanDao MStatusPengajuanDao;
    private static MNilaiJaminanDao MNilaiJaminanDao;
    private static MHeadPinjamanDao MHeadPinjamanDao;
    private static MDetailPinjamanDao MDetailPinjamanDao;
    private static MBuktiJaminanDao MBuktiJaminanDao;
    private static TmpDetailBrgDao TmpDetailBrgDao;
    private static MAddendumDao MAddendumDao;
    private static MDPinjamanDao MDPinjamanDao;
    private static MSetorTarikDao MSetorTarikDao;
    private static MProfileDao MProfileDao;

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public static MNasabahDao getmNasabahDao() {
        return mNasabahDao;
    }

    public static void setmNasabahDao(MNasabahDao mNasabahDao) {
        SpringInit.mNasabahDao = mNasabahDao;
    }

    public static MRcDao getmRcDao() {
        return mRcDao;
    }

    public static void setmRcDao(MRcDao mRcDao) {
        SpringInit.mRcDao = mRcDao;
    }

    public static MSettingDao getmSettingDao() {
        return mSettingDao;
    }

    public static void setmSettingDao(MSettingDao mSettingDao) {
        SpringInit.mSettingDao = mSettingDao;
    }

    public static MLogMobileDao getmLogMobileDao() {
        return mLogMobileDao;
    }

    public static void setmLogMobileDao(MLogMobileDao mLogMobileDao) {
        SpringInit.mLogMobileDao = mLogMobileDao;
    }

    public static MPengajuanDao getMPengajuanDao() {
        return MPengajuanDao;
    }

    public static void setMPengajuanDao(MPengajuanDao MPengajuanDao) {
        SpringInit.MPengajuanDao = MPengajuanDao;
    }

    public static MProductDao getMProductDao() {
        return MProductDao;
    }

    public static void setMProductDao(MProductDao MProductDao) {
        SpringInit.MProductDao = MProductDao;
    }

    public static MLogTrxDao getMLogTrxDao() {
        return MLogTrxDao;
    }

    public static void setMLogTrxDao(MLogTrxDao MLogTrxDao) {
        SpringInit.MLogTrxDao = MLogTrxDao;
    }

    public static MRoutingDao getMRoutingDao() {
        return MRoutingDao;
    }

    public static void setMRoutingDao(MRoutingDao MRoutingDao) {
        SpringInit.MRoutingDao = MRoutingDao;
    }

    public static TmpDetailChartDao getTmpDetailChartDao() {
        return TmpDetailChartDao;
    }

    public static void setTmpDetailChartDao(TmpDetailChartDao TmpDetailChartDao) {
        SpringInit.TmpDetailChartDao = TmpDetailChartDao;
    }

    public static MTransSPDao getMTransSPDao() {
        return MTransSPDao;
    }

    public static void setMTransSPDao(MTransSPDao MTransSPDao) {
        SpringInit.MTransSPDao = MTransSPDao;
    }

    public static MJurnalDao getMJurnalDao() {
        return MJurnalDao;
    }

    public static void setMJurnalDao(MJurnalDao MJurnalDao) {
        SpringInit.MJurnalDao = MJurnalDao;
    }

    public static MJenisSimpanDao getMJenisSimpanDao() {
        return MJenisSimpanDao;
    }

    public static void setMJenisSimpanDao(MJenisSimpanDao MJenisSimpanDao) {
        SpringInit.MJenisSimpanDao = MJenisSimpanDao;
    }

    public static MJenisAngsuranDao getMJenisAngsuranDao() {
        return MJenisAngsuranDao;
    }

    public static void setMJenisAngsuranDao(MJenisAngsuranDao MJenisAngsuranDao) {
        SpringInit.MJenisAngsuranDao = MJenisAngsuranDao;
    }

    public static MJenisPinjamanDao getMJenisPinjamanDao() {
        return MJenisPinjamanDao;
    }

    public static void setMJenisPinjamanDao(MJenisPinjamanDao MJenisPinjamanDao) {
        SpringInit.MJenisPinjamanDao = MJenisPinjamanDao;
    }

    public static MPresentasePlafonDao getMPresentasePlafonDao() {
        return MPresentasePlafonDao;
    }

    public static void setMPresentasePlafonDao(MPresentasePlafonDao MPresentasePlafonDao) {
        SpringInit.MPresentasePlafonDao = MPresentasePlafonDao;
    }

    public static MSukuBungaDao getMSukuBungaDao() {
        return MSukuBungaDao;
    }

    public static void setMSukuBungaDao(MSukuBungaDao MSukuBungaDao) {
        SpringInit.MSukuBungaDao = MSukuBungaDao;
    }

    public static MStatusPengajuanDao getMStatusPengajuanDao() {
        return MStatusPengajuanDao;
    }

    public static void setMStatusPengajuanDao(MStatusPengajuanDao MStatusPengajuanDao) {
        SpringInit.MStatusPengajuanDao = MStatusPengajuanDao;
    }

    public static TmpDetailBrgDao getTmpDetailBrgDao() {
        return TmpDetailBrgDao;
    }

    public static void setTmpDetailBrgDao(TmpDetailBrgDao TmpDetailBrgDao) {
        SpringInit.TmpDetailBrgDao = TmpDetailBrgDao;
    }

    public static MNilaiJaminanDao getMNilaiJaminanDao() {
        return MNilaiJaminanDao;
    }

    public static void setMNilaiJaminanDao(MNilaiJaminanDao MNilaiJaminanDao) {
        SpringInit.MNilaiJaminanDao = MNilaiJaminanDao;
    }

    public static MHeadPinjamanDao getMHeadPinjamanDao() {
        return MHeadPinjamanDao;
    }

    public static void setMHeadPinjamanDao(MHeadPinjamanDao MHeadPinjamanDao) {
        SpringInit.MHeadPinjamanDao = MHeadPinjamanDao;
    }

    public static MBuktiJaminanDao getMBuktiJaminanDao() {
        return MBuktiJaminanDao;
    }

    public static void setMBuktiJaminanDao(MBuktiJaminanDao MBuktiJaminanDao) {
        SpringInit.MBuktiJaminanDao = MBuktiJaminanDao;
    }

    public static MDetailPinjamanDao getMDetailPinjamanDao() {
        return MDetailPinjamanDao;
    }

    public static void setMDetailPinjamanDao(MDetailPinjamanDao MDetailPinjamanDao) {
        SpringInit.MDetailPinjamanDao = MDetailPinjamanDao;
    }

    public static MAddendumDao getMAddendumDao() {
        return MAddendumDao;
    }

    public static void setMAddendumDao(MAddendumDao MAddendumDao) {
        SpringInit.MAddendumDao = MAddendumDao;
    }

    public static MDPinjamanDao getMDPinjamanDao() {
        return MDPinjamanDao;
    }

    public static void setMDPinjamanDao(MDPinjamanDao MDPinjamanDao) {
        SpringInit.MDPinjamanDao = MDPinjamanDao;
    }

    public static MSetorTarikDao getMSetorTarikDao() {
        return MSetorTarikDao;
    }

    public static void setMSetorTarikDao(MSetorTarikDao MSetorTarikDao) {
        SpringInit.MSetorTarikDao = MSetorTarikDao;
    }

    public static MProfileDao getMProfileDao() {
        return MProfileDao;
    }

    public static void setMProfileDao(MProfileDao MProfileDao) {
        SpringInit.MProfileDao = MProfileDao;
    }

    
    
    
    public void initService() throws ConfigurationException {
//        ApplicationContext context = new FileSystemXmlApplicationContext("/src/main/resources/ApplicationContext.xml");
        ApplicationContext context = new FileSystemXmlApplicationContext("ApplicationContext.xml");
        
        setmNasabahDao(context.getBean("mNasabahDao", MNasabahDao.class));
        setmRcDao(context.getBean("mRcDao", MRcDao.class));
        setmSettingDao(context.getBean("mSettingDao", MSettingDao.class));
        setmLogMobileDao(context.getBean("MLogMobileDao", MLogMobileDao.class));
        setMPengajuanDao(context.getBean("MPengajuanDao", MPengajuanDao.class));
        setMProductDao(context.getBean("MProductDao", MProductDao.class));
        setMLogTrxDao(context.getBean("MLogTrxDao", MLogTrxDao.class));
        setMRoutingDao(context.getBean("MRoutingDao", MRoutingDao.class));
        setTmpDetailChartDao(context.getBean("TmpDetailChartDao", TmpDetailChartDao.class));
        setMTransSPDao(context.getBean("MTransSPDao", MTransSPDao.class));
        setMJurnalDao(context.getBean("MJurnalDao", MJurnalDao.class));
        setMJenisSimpanDao(context.getBean("MJenisSimpanDao", MJenisSimpanDao.class));
        setMJenisAngsuranDao(context.getBean("MJenisAngsuranDao", MJenisAngsuranDao.class));
        setMJenisPinjamanDao(context.getBean("MJenisPinjamanDao", MJenisPinjamanDao.class));
        setMPresentasePlafonDao(context.getBean("MPresentasePlafonDao", MPresentasePlafonDao.class));
        setMSukuBungaDao(context.getBean("MSukuBungaDao", MSukuBungaDao.class));
        setMStatusPengajuanDao(context.getBean("MStatusPengajuanDao", MStatusPengajuanDao.class));
        setTmpDetailBrgDao(context.getBean("TmpDetailBrgDao", TmpDetailBrgDao.class));
        setMNilaiJaminanDao(context.getBean("MNilaiJaminanDao", MNilaiJaminanDao.class));
        setMHeadPinjamanDao(context.getBean("MHeadPinjamanDao", MHeadPinjamanDao.class));
        setMBuktiJaminanDao(context.getBean("MBuktiJaminanDao", MBuktiJaminanDao.class));
        setMDetailPinjamanDao(context.getBean("MDetailPinjamanDao", MDetailPinjamanDao.class));
        setMAddendumDao(context.getBean("MAddendumDao", MAddendumDao.class));
        setMDPinjamanDao(context.getBean("MDPinjamanDao", MDPinjamanDao.class));
        setMSetorTarikDao(context.getBean("MSetorTarikDao", MSetorTarikDao.class));
        setMProfileDao(context.getBean("MProfileDao", MProfileDao.class));
        
        log.info("Init DB has successfull");
        log.info("Service Started");

    }
}
