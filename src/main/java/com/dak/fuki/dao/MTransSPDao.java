/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.dao;

import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MTransSP;
import com.dak.fuki.entity.TmpDetailChart;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Frika Da Cintia
 */
@Repository(value = "MTransSPDao")
@Transactional
public class MTransSPDao extends Dao{
    
    public List<MTransSP> getAll(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id ORDER BY mt.id ASC")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<Object[]> getAllByDate() {
        try {
               return em.createNativeQuery("SELECT * FROM tbl_trans_sp WHERE anggota_id = '3' and tgl_transaksi >= DATE('2019-07-03') and tgl_transaksi <= DATE('2019-07-05')")
//                    .setParameter("anggota_id", anggota_id)
//                    .setParameter("strdate", strdate)
//                    .setParameter("enddate", enddate)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MTransSP> getAllSetoran(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MTransSP> getAllPenarikan(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MTransSP> getSetorSimpananWajib(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='41' AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getTarikSimpananWajib(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='41' AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getSetorSimpananPokok(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='40' AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getTarikSimpananPokok(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='40' AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getSetorSimpananSukarela(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='32' AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getTarikSimpananSukarela(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='32' AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

}
