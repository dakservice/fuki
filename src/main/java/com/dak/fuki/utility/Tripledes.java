package com.dak.fuki.utility;

import java.io.IOException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

/**
 *
 * @author Frika Da Cintia
 */
public class Tripledes {

   private DESedeKeySpec desKeySpec;
	private IvParameterSpec ivSpec;
	
	public Tripledes(String key) {
		try {
			byte[] keyBytes = key.getBytes();
			this.desKeySpec = new DESedeKeySpec(keyBytes);
			this.ivSpec = new IvParameterSpec(key.substring(0, 8).getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String encrypt(String origData) {
		try {
                        byte[] ori = origData.getBytes();
			SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
			SecretKey key = factory.generateSecret(this.desKeySpec);
			Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key, this.ivSpec);
			byte[] crypted = cipher.doFinal(ori);
                        return base64Encode(crypted);
		}  catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String decrypt(String crypted) {
		try {
                        byte[] crypt = base64Decode(crypted);
			SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
			SecretKey key = factory.generateSecret(this.desKeySpec);
			Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, this.ivSpec);
//			return cipher.doFinal(crypted);
                        return new String(cipher.doFinal(crypt));
		}  catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String base64Encode(byte[] data) {
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(data);
	}
        private static byte[] base64Decode(String data) throws IOException{
            BASE64Decoder decoder = new BASE64Decoder();
            return decoder.decodeBuffer(data);
        }
}
