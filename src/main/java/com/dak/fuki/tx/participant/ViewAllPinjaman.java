/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.tx.participant;

import com.dak.fuki.entity.MHeadPinjaman;
import com.dak.fuki.entity.MJenisPinjaman;
import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MPengajuan;
import com.dak.fuki.entity.MProduct;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.MStatusPengajuan;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class ViewAllPinjaman implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    
    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        JSONArray array = new JSONArray();
        JSONArray arrays = new JSONArray();
        JSONArray json_ = new JSONArray();
        DecimalFormat df = new DecimalFormat("#");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Date tgl_tempo;
        

        try {
            
            log.info("================= MASUK TRY ");
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
//            log.info("================= MASUK NASABAH " +nsbh.getAktif());
            
            if(nsbh != null){
                if(nsbh.getAktif().equals("Y")){
                
                    log.info("================= MASUK IF ");
                    JSONArray data = new JSONArray();
                    log.info("================= SEBELUM LIST.");
                    List<MHeadPinjaman> mp = SpringInit.getMHeadPinjamanDao().getAllbyIdAnggota(nsbh.getId().intValue());
                    
                        if(mp.size() > 0){
                            log.info("================= SEBELUM LIST. DATA: "+mp.size());
                            log.info("================= MASUK LIST ");
                            for (MHeadPinjaman datamp : mp) {
                                JSONObject logData = new JSONObject();
                                log.info("==================== JENIS PINJAMAN "+datamp.getJenis_pinjaman());
                                MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(datamp.getJenis_pinjaman());
                                logData.put("id", datamp.getId());
                                logData.put("jenis", mjp.getNama_pinjaman());
//                                String date = sdf.format(datamp.getTgl_pinjam().toString());
                                Date date = sdf.parse(datamp.getTgl_pinjam().toString());
                                String tgl = sdf.format(date);
                                logData.put("tanggalpinjam", tgl);
                                logData.put("lamaangsuran", datamp.getLama_angsuran().toString());
                                logData.put("jumlah", datamp.getJumlah().toString());
    //                        int jasa = (datamp.getBunga().intValue() /100) * datamp.getJumlah() / datamp.getLama_angsuran();
    //                        log.info("================= BUNGA "+jasa);
                                logData.put("jasa", df.format(datamp.getBunga()).toString()+"%");
                                logData.put("administrasi", datamp.getBiaya_adm().toString());
                                logData.put("provisi", datamp.getBiaya_provisi().toString());
                                cal.add(Calendar.DAY_OF_MONTH, 30 * datamp.getLama_angsuran());
                                tgl_tempo = cal.getTime();
                                String tgl_pinjam = datamp.getTgl_pinjam().substring(8, 10);
                                logData.put("jatuhtempo", sdf.format(tgl_tempo).toString().substring(0, 8)+tgl_pinjam);
                                String l;
                                if(datamp.getLunas() == "Lunas"){
                                    l = "Sudah Lunas";
                                }else{
                                    l = "Belum Lunas";
                                }
                                logData.put("lunas", l);
                                logData.put("keterangan", datamp.getKeterangan());

                                array.put(logData);

                            }
                            JSONObject json2 = new JSONObject();
                            json2.put("data", array);
                            log.info("=============================== LOG DATA :"+json2);

                            rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(),json2);
                        }else{
                            rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                        }
                }
                else{
                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_AUTH_FAILED);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            }else{
                rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++"+stringWriter.toString());
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
