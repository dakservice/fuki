/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.simpanan;

import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.MTokenSetorTarik;
import com.dak.fuki.entity.MTokenSetorTarikDetail;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import com.dak.fuki.utility.Utility;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class TokenSetoran implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    
    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    Calendar cal = Calendar.getInstance();
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        Date expire;
        
        try {
            String token = Utility.generateToken(6);
            
            cal.add(Calendar.MINUTE, 30);
            expire = cal.getTime();
            log.info("=========== EXPIRE GOBLOK :"+expire);
            Integer values = 0;
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            JSONArray logSetoran = jsonReq.getJSONArray("setoran");
            int lengthS = logSetoran.length();
                if (lengthS > 0) {
                    try {
                        JSONArray jarr = new JSONArray();
                        
                        MTokenSetorTarik mtarik = new MTokenSetorTarik();
                        mtarik.setToken_number(token);
                        mtarik.setExpired_date(time.format(expire));
                        mtarik.setAction("SETORAN");
                        mtarik.setStatus("0");
                        mtarik.setId_user(nsbh.getId().toString());
                        mtarik.setTanggal_trx(datetime.format(new Date()));

                        SpringInit.getMSetorTarikDao().saveOrUpdate(mtarik);
                        
                        for (int a = 0; a < lengthS; a++) {
                        JSONObject jso = new JSONObject();
                        JSONObject setorans = logSetoran.getJSONObject(a);
                        String id_simpanan = setorans.getString("id_simpanan");
                        String value = setorans.getString("value");
                        
                       
                        
                        MTokenSetorTarikDetail mtarikdetail = new MTokenSetorTarikDetail();
                        mtarikdetail.setToken_id(mtarik.getId());
                        mtarikdetail.setId_simpanan(Integer.parseInt(id_simpanan));
                        mtarikdetail.setNominal(Integer.parseInt(value));

                        SpringInit.getMSetorTarikDao().saveOrUpdate(mtarikdetail);

                        
                        values += Integer.parseInt(value);
                        }
                        JSONObject js = new JSONObject();
                        js.put("total_pembayaran", values.toString());
                        js.put("token", token);
                        js.put("expire_date", time.format(expire));

                        JSONObject json = new JSONObject();
                        json.put("data", js);
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);
                    } catch (Exception e) {
                    }
                }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
        
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
