/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.simpanan;

import com.dak.fuki.entity.MJenisSimpan;
import com.dak.fuki.entity.MLogTrx;
import com.dak.fuki.entity.MNasabah;
import com.dak.fuki.entity.MRc;
import com.dak.fuki.entity.MTransSP;
import com.dak.fuki.spring.SpringInit;
import com.dak.fuki.utility.Constant;
import com.dak.fuki.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Frika Da Cintia
 */
public class initSaldo implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DecimalFormat df = new DecimalFormat("#");
        JSONArray arrays = new JSONArray();
        
        try {
            String notelp = jsonReq.getString("notelp");
            
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);           

            if (nsbh == null) {
                log.info("====================== DATA" +nsbh);
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            } else {
                List<MJenisSimpan> mjss = SpringInit.getMJenisSimpanDao().getJenisSimpanan();
                for (MJenisSimpan datams : mjss) {
                    JSONObject logData = new JSONObject();
                        logData.put("id", datams.getId());
                        logData.put("jenis_tabungan", datams.getJns_simpan());
                        
                        Double saldo = SpringInit.getmNasabahDao().getSaldoByIdSimpanan(nsbh.getId().toString(), datams.getId().toString());
                        if(saldo > 0){
                            logData.put("saldo", df.format(saldo).toString());
                        }else{
                            logData.put("saldo", "0");
                        }
                        
                        
                        arrays.put(logData);
                }
                
//                String id_simpanan = jsonReq.getString("id_simpanan");
//                Double saldo = SpringInit.getmNasabahDao().getSaldoByIdSimpanan(nsbh.getId().toString(), id_simpanan);
                
                JSONObject json = new JSONObject();
                json.put("jenis_simpanan", arrays);
//                json.put("saldo", df.format(saldo).toString());
                
                JSONObject json2 = new JSONObject();
                json2.put("data", json);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
                
            }
        
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++"+stringWriter.toString());
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
