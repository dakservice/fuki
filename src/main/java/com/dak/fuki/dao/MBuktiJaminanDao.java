/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fuki.dao;

import com.dak.fuki.entity.MBuktiJaminan;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Frika Da Cintia
 */
@Repository(value = "MBuktiJaminanDao")
@Transactional
public class MBuktiJaminanDao extends Dao{
    
        public MBuktiJaminan saveOrUpdate(MBuktiJaminan mBuktiJaminan) {
        if (mBuktiJaminan.getId() == null) {
            em.persist(mBuktiJaminan);
        } else {
            em.merge(mBuktiJaminan);
        }
        return mBuktiJaminan;
    }
    
}
